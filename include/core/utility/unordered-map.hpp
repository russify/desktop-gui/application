#pragma once

#include <functional>
#include <unordered_map>
#include <utility>

#include "common/include/utility/memory/allocator.hpp"

namespace core::utility {

template <class Key, class Value, class Hasher = std::hash<Key>, class KeyComparator = std::equal_to<Key>,
          class Allocator = ::utility::memory::Allocator<std::pair<const Key, Value>>>
using UnorderedMap = std::unordered_map<Key, Value, Hasher, KeyComparator, Allocator>;
}

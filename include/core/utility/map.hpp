#pragma once

#include <functional>
#include <map>
#include <utility>

#include "common/include/utility/memory/allocator.hpp"

namespace core::utility {
template <class Key, class Value, class KeyComparator = std::less<Key>,
          class Allocator = ::utility::memory::Allocator<std::pair<const Key, Value>>>
using Map = std::map<Key, Value, KeyComparator, Allocator>;
}

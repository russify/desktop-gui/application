#pragma once

// std
#include <functional>
#include <stdexcept>
#include <utility>

// containers
#include "./vector.hpp"

// boost
#include <boost/asio/executor_work_guard.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/post.hpp>
#include <boost/thread.hpp>

namespace core::utility {
// Simple pool of workers built on `io_context::run`
class ThreadPool {
 public:
  // `pool_size` - number of created workers
  explicit ThreadPool(std::uint8_t pool_size);
  ~ThreadPool();

 public:
  // Adds task to the thread pool
  template <class Task>
  inline void PushTask(Task &&task) {
    if (ctx_.stopped()) {
      throw std::runtime_error{"thread pool is stopped"};
    }

    boost::asio::post(ctx_, std::forward<Task>(task));
  }
  // Stops thread pool
  inline void StopPool() {
    if (ctx_.stopped()) {
      return;
    }

    ctx_.stop();
    workers_.join_all();
  }

 public:
  // Returns underlying `io_context`
  [[nodiscard]] inline boost::asio::io_context &GetIO() noexcept { return ctx_; }

 private:
  boost::asio::io_context ctx_;
  boost::asio::executor_work_guard<decltype(ctx_.get_executor())> guard_;

  boost::thread_group workers_;
};
}  // namespace core::utility

#pragma once

#include <vector>

#include "common/include/utility/memory/allocator.hpp"

namespace core::utility {
template <class T, class Allocator = ::utility::memory::Allocator<T>>
using Vector = std::vector<T>;
}

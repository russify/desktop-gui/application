#pragma once
// qt
#include <qstring.h>

// current
#include "core/utility/string.hpp"

namespace core::utility {
[[nodiscard]] String ToStdString(const QString &data);
}  // namespace core::utility

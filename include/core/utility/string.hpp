#pragma once

#include <string>

#include "common/include/utility/memory/allocator.hpp"

namespace core::utility {
using String = std::basic_string<char, std::char_traits<char>, ::utility::memory::Allocator<char>>;
}

#pragma once
// std
#include <tuple>

// boost
#include <boost/thread/lock_guard.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/shared_lock_guard.hpp>
#include <boost/thread/shared_mutex.hpp>

namespace core::utility {
template <class T, bool shared = false>
class GuardedValue {
 public:
  [[nodiscard]] std::tuple<boost::lock_guard<boost::mutex> &&, const T &> Get() const {
    return {boost::shared_lock_guard{guard_}, value_};
  }

  [[nodiscard]] std::tuple<boost::lock_guard<boost::mutex> &&, T &> GetExclusive() {
    return {boost::shared_lock_guard{guard_}, value_};
  }

 private:
  T value_;
  mutable boost::mutex guard_;
};

template <class T>
class GuardedValue<T, true> {
 public:
  [[nodiscard]] std::tuple<boost::shared_lock_guard<boost::shared_mutex> &&, const T &> Get() const {
    return {boost::shared_lock_guard{guard_}, value_};
  }

  [[nodiscard]] std::tuple<boost::shared_lock_guard<boost::shared_mutex> &&, T &> GetExclusive() {
    return {boost::shared_lock_guard{guard_}, value_};
  }

 private:
  T value_;
  mutable boost::shared_mutex guard_;
};
}  // namespace core::utility

#pragma once

#include <nlohmann/json.hpp>
#include <utility/memory/allocator.hpp>

#include "./map.hpp"
#include "./string.hpp"
#include "./vector.hpp"

namespace core::utility {
using Json = nlohmann::basic_json<
    Map, Vector, String, bool, std::int64_t, std::uint64_t, double,                 // NOLINT build/include_what_you_use
    ::utility::memory::Allocator, nlohmann::adl_serializer, Vector<std::uint8_t>>;  // NOLINT build/include_what_you_use
}  // namespace core::utility

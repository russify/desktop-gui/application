#pragma once
#include <cstdint>

namespace core::utility {
template <class T, std::uint64_t Size>
[[nodiscard]] consteval std::uint64_t ArraySize(const T (&)[Size]) noexcept {
  return Size;
}
}  // namespace core::utility

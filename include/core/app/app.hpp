#pragma once

#include "./context.hpp"
#include "ui/player/player.hpp"

namespace core::app {
class Application {
 public:
  enum ReturnCode { kSuccess, kFail, kRestart };

  [[nodiscard]] ReturnCode Run(int argc, char **argv);

 private:
  Context context_;
};
}  // namespace core::app

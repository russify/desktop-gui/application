#pragma once
#include "core/utility/guarded-value.hpp"
#include "core/utility/thread-pool.hpp"
#include "proto-files/generated/session-info.pb.h"

#ifndef MIN
#define UNDEF_CUSTOM_MIN 1
#define MIN(lhs, rhs) ((lhs) < (rhs) ? (lhs) : (rhs))
#endif  // !MIN

namespace core::app {
// Contains application resources
class Context {
 public:
  explicit Context(std::uint8_t pool_size = MIN(2, std::thread::hardware_concurrency()));

 public:
  [[nodiscard]] utility::ThreadPool &GetPool() noexcept;

  void SetSession(const proto::SessionInfo &info);
  [[nodiscard]] const proto::SessionInfo &Session() const noexcept;

 private:
  utility::ThreadPool pool_;

  utility::GuardedValue<proto::SessionInfo, true> session_;
};
}  // namespace core::app

#ifdef UNDEF_CUSTOM_MIN
#undef MIN
#endif

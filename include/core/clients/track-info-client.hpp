#pragma once
// protos
#include <track-info.pb.h>

// containers
#include <core/utility/string.hpp>

namespace core::clients {
// Client that allows to retrive track information
class TrackInfoClient {
 public:
  // TODO(shmuk): stub
  [[nodiscard]] utility::String GetTracks(std::size_t limit, std::size_t offset);
};
}  // namespace core::clients

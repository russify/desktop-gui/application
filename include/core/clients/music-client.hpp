#pragma once
#include "core/utility/vector.hpp"

// boost
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/atomic/atomic_flag.hpp>
#include <boost/beast/websocket.hpp>

namespace core::clients {
class MusicClient {
 public:
  // `context` must be available until client descruction
  // `max_retry_count` - maximum number or reconnections if `Connect` failed
  explicit MusicClient(boost::asio::io_context *context, std::uint8_t max_connect_retry_count = 1);
  ~MusicClient();

 public:
  void Connect();
  void Disconnect();

 public:
  [[nodiscard]] utility::Vector<char> NextPacket(std::size_t track_id, std::size_t start_pos, std::size_t end_pos);
  [[nodiscard]] bool IsConnected() noexcept;

 private:
  boost::asio::io_context *context_;
  boost::beast::websocket::stream<boost::asio::ip::tcp::socket> stream_;

  std::uint8_t max_connect_retry_count_;

  boost::atomic_flag connection_in_process_;
};
}  // namespace core::clients

#pragma once
// protos
#include "proto-files/generated/location.pb.h"

// std
#include <string_view>

// project
#include "core/utility/vector.hpp"

namespace core::clients {
class LocationClient {
 public:
  LocationClient();

 public:
  [[nodiscard]] const utility::Vector<proto::Country> &GetCountries() const noexcept;
  [[nodiscard]] utility::Vector<proto::Location> GetLocations(std::string_view country_name);

 public:
  void ReadCountries();

 private:
  utility::Vector<proto::Country> countries_;
};
}  // namespace core::clients

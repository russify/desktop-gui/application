#pragma once
#include <string_view>

// boost
#include <boost/atomic/atomic.hpp>

#include "core/app/context.hpp"

// proto
#include "proto-files/generated/session-info.pb.h"
#include "proto-files/generated/user-info.pb.h"

namespace core::clients {
class AuthClient {
 public:
  explicit AuthClient(app::Context *context) noexcept;

 public:
  void Authenticate(std::string_view username, std::string_view password);
  [[nodiscard]] bool IsAuthenticated() const noexcept;
  [[nodiscard]] bool IsSessionExpired() const noexcept;

  void Register(const proto::UserRegistrationInfo &info);

 private:
  void SaveSession(const proto::SessionInfo &session);
  bool ReadSession();

 private:
  app::Context *context_;
  boost::atomic_bool authenticated_;
};
}  // namespace core::clients

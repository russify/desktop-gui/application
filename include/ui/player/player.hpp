#pragma once

// qt
#include <qbuffer.h>
#include <qmediaplayer.h>
#include <qobject.h>

// common
#include "core/app/context.hpp"

// clients
#include "core/clients/music-client.hpp"

// boost
#include <boost/asio/strand.hpp>
#include <boost/atomic/atomic.hpp>
#include <boost/atomic/atomic_flag.hpp>
#include <boost/thread/mutex.hpp>

namespace ui::player {
class Player : public QObject {
  Q_OBJECT

 public:
  explicit Player(core::app::Context *context);

 public slots:
  void StartFrom(std::uint16_t start_position);
  void Pause();
  void Resume();

 public slots:
  void SetCurrentTrack(std::size_t track_id, std::size_t length) noexcept;
  void ChangeVolume(std::uint8_t volume);

 signals:
  // all signals have to start from lower

  // when current position changed
  void playbackPositionChanged(double position_seconds);
  // New data packet arrived
  void newDataLoaded(quint64 position_millis);
  // Start downloading new packets from some position
  void startDownloading();
  // Something went wrong during listening
  void fail(const QString &what);

 private:
  void StartFromImpl(std::uint16_t start_position);

 private:
  [[nodiscard]] std::size_t ReadSinglePacket(std::size_t track_id, std::size_t start_position);

 private:
  void AwaitDownloadTask();
  void DownloadPackets(std::uint16_t start_position);

  void CheckPlayerState();

 private:
  core::app::Context *ctx_;

  std::size_t current_track_id_{};
  std::size_t length_{};

  boost::atomic<bool> force_stop_;     // stop downloading current track
  boost::atomic_flag is_task_active_;  // does we already pushed downloading task in pool
  double offset_{};
  quint64 last_pos_{};

  boost::asio::io_context::strand strand_;
  core::clients::MusicClient client_;

  QBuffer data_;
  QMediaPlayer player_;

 public:
  static inline std::uint16_t max_packet_size{5};
};
}  // namespace ui::player

#pragma once

// qt
#include <qobject.h>
#include <qstring.h>

// current project
#include "core/clients/track-info-client.hpp"
#include "core/utility/string.hpp"

namespace ui::tracks {
class TrackInfoClient : public QObject {
  Q_OBJECT

 public:
  // TODO(shmuk): stub
  Q_INVOKABLE void GetTracks(std::size_t limit, std::size_t offset);

 signals:
  void trackInfoArrived(const QString &json);

 private:
  core::clients::TrackInfoClient internal_client_;
};
}  // namespace ui::tracks

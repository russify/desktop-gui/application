#pragma once
#include <qobject.h>  // NOLINT

// current
#include "core/app/context.hpp"
#include "core/clients/auth-client.hpp"

// QT
#include "qstring.h"  // NOLINT

namespace ui::authentication {
class AuthClient : public QObject {
  Q_OBJECT;

 public:
  explicit AuthClient(core::app::Context *context) noexcept;

 public:
  Q_INVOKABLE void Authenticate(const QString &username, const QString &password) noexcept;
  Q_INVOKABLE bool IsAuthenticated() const noexcept;

  Q_INVOKABLE void Register(const QString &data);

 signals:
  void success();
  void failure();

  void registrationFailed();
  void registrationSucceed();

 private:
  core::clients::AuthClient client_;
};
}  // namespace ui::authentication

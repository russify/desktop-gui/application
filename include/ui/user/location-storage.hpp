#pragma once
// qt
#include <qobject.h>
#include <qstring.h>

// curent
#include "core/clients/location-client.hpp"

namespace ui::user {
class LocationStorage : public QObject {
  Q_OBJECT
 public:
  Q_INVOKABLE QString GetCountries();
  Q_INVOKABLE QString GetLocations(const QString &country);

 private:
  core::clients::LocationClient client_;
};
}  // namespace ui::user

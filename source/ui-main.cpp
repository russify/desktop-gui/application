// clang-format off
#include <core/pch.hpp>
// clang-format on
#include <core/app/app.hpp>

// qt
#include <qdebug.h>  // NOLINT

int main(int argc, char **argv) {
  try {
    core::app::Application app;
    return app.Run(argc, argv);
  } catch (const std::exception &ex) {
    // TODO(shmuk): log ex
    qDebug() << ex.what();
    return core::app::Application::ReturnCode::kFail;
  } catch (...) {
    // TODO(shmuk): log here
    return core::app::Application::ReturnCode::kFail;
  }
}

// clang-format off
#include "core/pch.hpp"
// clang-format on

#include "core/clients/auth-client.hpp"

#include "core/utility/utils.hpp"

// memory
#include "utility/memory/allocator.hpp"

// std
#include <format>   // NOLINT
#include <fstream>  // NOLINT

// boost
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/core/flat_buffer.hpp>
#include <boost/beast/http.hpp>
#include <boost/filesystem.hpp>

using namespace boost::asio;  // NOLINT
using namespace ip;           // NOLINT

using namespace boost::beast;  // NOLINT

namespace core::clients {
AuthClient::AuthClient(app::Context *context) noexcept : context_{context} {
  authenticated_.store(ReadSession(), boost::memory_order_relaxed);
}

void AuthClient::Authenticate(std::string_view username, std::string_view password) {
  static constexpr char host[]{"192.168.0.101"};
  static constexpr char port[]{"8000"};

  /*const auto resolution{tcp::resolver{context_->GetPool().GetIO()}.resolve(host, port)};
  tcp::socket socket{context_->GetPool().GetIO()};
  for (const auto &endpoint : resolution) {
    socket.connect(endpoint);
  }

  http::request<http::string_body> request;
  request.set(http::field::host, host);
  request.set(http::field::user_agent, "app");
  request.method(http::verb::get);
  request.target("/login");
  request.body() = std::format("{{\"username\": \"{}\", \"password\": \"{}\"}}", username, password);
  request.prepare_payload();
  http::write(socket, request);

  basic_flat_buffer<::utility::memory::Allocator<char>> buffer;
  http::response<http::dynamic_body> response;
  http::read(socket, buffer, response);*/

  if (username != "shamil") {
    throw std::invalid_argument{"blabla"};
  }

  if (password != "12345678") {
    throw std::invalid_argument{"aaaaa"};
  }

  proto::SessionInfo session;
  session.SetUsername(username.data());
  /*for (const auto &cookie : http::param_list(response[http::field::cookie])) {
    if (cookie.first == "session-id") {
      session.SetSessionId(cookie.second.data());
      break;
    }
  }*/
  // TODO(shmuk): delete me!
  session.SetSessionId("abracadabra");

  if (!session.HasSessionId()) {
    throw std::runtime_error{"authentication failed"};
  }
  context_->SetSession(session);
  SaveSession(session);
  authenticated_.store(true, boost::memory_order_release);
}

[[nodiscard]] bool AuthClient::IsAuthenticated() const noexcept {
  return authenticated_.load(boost::memory_order_acquire);
}
[[nodiscard]] bool AuthClient::IsSessionExpired() const noexcept { return false; }

void AuthClient::Register(const proto::UserRegistrationInfo &info) {
  if (!info.GetLocation().HasName() || info.GetLocation().Name().empty()) {
    throw std::invalid_argument{"empty location"};
  }

  proto::SessionInfo session;
  session.SetUsername(info.Email());
  session.SetSessionId("id");
  SaveSession(session);
  authenticated_.store(true, boost::memory_order_release);
}

void AuthClient::SaveSession(const proto::SessionInfo &session) {
  if (!(boost::filesystem::exists("tmp") && boost::filesystem::is_directory("tmp"))) {
    boost::filesystem::create_directory("tmp");
  }

  std::ofstream file{"./tmp/session", std::ios::trunc | std::ios::binary};
  const auto data{nlohmann::json::to_bson(session.ToJson())};
  for (auto byte : data) {
    file << byte;
  }
}
bool AuthClient::ReadSession() {
  try {
    std::ifstream file{"tmp/session", std::ios::binary};
    if (!file.is_open()) {
      return false;
    }

    char chunk[128];
    std::vector<std::uint8_t> data;
    std::streamsize read;
    do {
      file.read(chunk, utility::ArraySize(chunk));
      read = file.gcount();

      data.reserve(data.size() + read);
      for (decltype(read) index{}; index < read; ++index) {
        data.emplace_back(reinterpret_cast<std::uint8_t &>(chunk[index]));
      }
    } while (read == utility::ArraySize(chunk));

    proto::SessionInfo session;
    session.FromJson(nlohmann::json::from_bson(data));
    context_->SetSession(session);
    return true;
  } catch (const std::exception &) {
    return false;
  }
}
}  // namespace core::clients

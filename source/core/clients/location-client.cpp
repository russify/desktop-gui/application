// clang-format off
#include "core/pch.hpp" // NOLINT
// clang-format on

#include "core/clients/location-client.hpp"

// yaml
#include "yaml-cpp/yaml.h"

namespace core::clients {
LocationClient::LocationClient() try { ReadCountries(); } catch (const std::exception &) {
}

[[nodiscard]] const utility::Vector<proto::Country> &LocationClient::GetCountries() const noexcept {
  return countries_;
}

[[nodiscard]] utility::Vector<proto::Location> LocationClient::GetLocations(std::string_view country_name) try {
  YAML::Node data{YAML::LoadFile("tmp/locations.yaml")};

  utility::Vector<proto::Location> result;
  auto countries{data["countries"]};
  for (const auto &country_node : countries) {
    if (country_node["name"].as<std::string>().compare(country_name.data()) == 0) {
      const auto locations{country_node["locations"]};
      if (!locations.IsSequence()) {
        return result;
      }

      result.reserve(locations.size());
      for (const auto &location_node : locations) {
        proto::Location location;
        location.SetName(location_node["name"].as<std::string>().data());
        result.emplace_back(std::move(location));
      }
    }
  }
  return result;
} catch (const std::exception &) {
  return {};
}

void LocationClient::ReadCountries() {
  YAML::Node data{YAML::LoadFile("tmp/locations.yaml")};

  auto countries{data["countries"]};
  if (!countries.IsSequence()) {
    return;
  }

  countries_.reserve(countries.size());
  for (const auto &country_node : countries) {
    proto::Country country;
    country.SetName(country_node["name"].as<std::string>().data());
    countries_.emplace_back(std::move(country));
  }
}
}  // namespace core::clients

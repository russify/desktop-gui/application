// clang-format off
#include "core/pch.hpp"
// clang-format on

#include "core/clients/music-client.hpp"

#include "core/utility/json.hpp"

// boost
#include <boost/asio/connect.hpp>
#include <boost/beast/core/buffers_to_string.hpp>
#include <boost/beast/core/role.hpp>

using tcp = boost::asio::ip::tcp;
namespace asio = boost::asio;
namespace websocket = boost::beast::websocket;

namespace core::clients {
MusicClient::MusicClient(boost::asio::io_context *context, std::uint8_t max_connect_retry_count)
    : context_{context}, stream_{*context_}, max_connect_retry_count_{max_connect_retry_count} {}

MusicClient::~MusicClient() {
  connection_in_process_.wait(true, boost::memory_order_acquire);
  Disconnect();
}

void MusicClient::Connect() {
  if (IsConnected()) {
    return;
  }

  auto cleaner = [this](boost::atomic_flag *flag) {
    flag->clear(boost::memory_order_release);
    flag->notify_all();
  };
  std::unique_ptr<boost::atomic_flag, decltype(cleaner)> auto_clean{&connection_in_process_, std::move(cleaner)};

  connection_in_process_.test_and_set(boost::memory_order_release);

  stream_.set_option(websocket::stream_base::timeout::suggested(boost::beast::role_type::client));

#define HOST "192.168.0.192"
#define PORT "8000"
  std::uint8_t retry_count{};
  while (true) {
    try {
      asio::connect(stream_.next_layer(), tcp::resolver{*context_}.resolve(HOST, PORT));
      stream_.handshake(HOST ":" PORT, "/api/v1/music/data");
      break;
    } catch (const std::exception &ex) {
      if (++retry_count == max_connect_retry_count_ || context_->stopped()) {
        throw;
      }
    }
  }
#undef HOST
#undef PORT
}

void MusicClient::Disconnect() {
  if (stream_.is_open()) {
    stream_.close(websocket::close_code::normal);
  }
}

utility::Vector<char> MusicClient::NextPacket(std::size_t track_id, std::size_t start_pos, std::size_t end_pos) {
  utility::Json request;
  request["id"] = track_id;
  request["start"] = start_pos;
  request["end"] = end_pos;
  stream_.write(asio::buffer(request.dump()));

  boost::beast::basic_flat_buffer<::utility::memory::Allocator<char>> buffer;
  stream_.read(buffer);
  return utility::Vector<char>{reinterpret_cast<const char *>(buffer.data().data()),
                               reinterpret_cast<const char *>(buffer.data().data()) + buffer.size()};
}

bool MusicClient::IsConnected() noexcept {
  try {
    if (!stream_.is_open()) {
      return false;
    }

    stream_.ping({});
    return true;
  } catch (const std::exception &ex) {
    return false;
  }
}
}  // namespace core::clients

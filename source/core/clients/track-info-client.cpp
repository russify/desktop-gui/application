#include <core/clients/track-info-client.hpp>
#include <random>  // TOOD(shmuk): DELETE ME!

// utility
#include <core/utility/vector.hpp>

namespace core::clients {
namespace {
template <std::size_t Size, class T>
[[nodiscard]] consteval std::size_t GetSize(T (&)[Size]) {
  static_assert(Size > 0, "WTF?");
  return Size;
}

// TODO(shmuk): DELETE ME!!
[[nodiscard]] proto::TrackInfo GenerateRandomValue() {
  constexpr const char *const kWordPool[]{"First", "Second", "Third", "Fourth", "Fifth", "Sixth"};
  constexpr auto kWordPoolSize{GetSize(kWordPool)};

  constexpr const char *const kUrlPool[]{
      "https://cdn.esawebb.org/archives/images/screen/weic2216b.jpg",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdv0tlExCZFVN51-16avxNr1fjvafc4h1ozFenUqEOjw&s",
      "https://images.ctfassets.net/hrltx12pl8hq/01rJn4TormMsGQs1ZRIpzX/02e9885a9ae69312da844bc58eedced1/"
      "Artboard_Copy_22.png?fit=fill&w=600&h=400"};
  constexpr auto kUrlPoolSize{GetSize(kUrlPool)};

  std::random_device rd;
  proto::TrackInfo result;
  result.SetYear(2000 + rd() % 23);
  result.SetDuration(60 + rd() % 300);
  result.SetImageUrl(kUrlPool[rd() % kUrlPoolSize]);

#define GENERATE(field_name)                                                                     \
  for (unsigned char index{}, length{1 + rd() % (kWordPoolSize - 1)}; index < length; ++index) { \
    result.Set##field_name(result.##field_name() + kWordPool[rd() % kWordPoolSize]);             \
  }

  GENERATE(Album);
  GENERATE(Name);
  GENERATE(Group);

#undef GENERATE

  for (unsigned char index{}, length{rd() % kWordPoolSize}; index < length; ++index) {
    result.SetName(result.Name() + kWordPool[rd() % kWordPoolSize]);
  }

  return result;
}
}  // namespace

utility::String TrackInfoClient::GetTracks(std::size_t limit, std::size_t offset) {
  utility::String result;
  result += "{ \"data\": [";
  for (std::size_t index{}; index < limit - 1; ++index) {  // NOLINT
    result += GenerateRandomValue().ToJson().dump();
    result += ',';
  }
  result += GenerateRandomValue().ToJson().dump();
  result += "]}";

  return result;
}
}  // namespace core::clients

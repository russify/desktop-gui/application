#include "core/utility/thread-pool.hpp"

namespace core::utility {
ThreadPool::ThreadPool(std::uint8_t pool_size) : ctx_{}, guard_{ctx_.get_executor()} {
  for (std::uint8_t counter{}; counter < pool_size; ++counter) {
    workers_.create_thread([&io = ctx_]() { io.run(); });
  }
}

ThreadPool::~ThreadPool() { StopPool(); }
}  // namespace core::utility

#include "core/utility/qt-helpers.hpp"

namespace core::utility {
[[nodiscard]] String ToStdString(const QString &data) {
  String result;
  result.reserve(data.size());
  for (auto ch : data) {
    result.push_back(ch.toLatin1());
  }

  return result;
}
}  // namespace core::utility

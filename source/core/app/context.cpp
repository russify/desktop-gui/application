// clang-format off
#include "core/pch.hpp"
// clang-format on

#include "core/app/context.hpp"

namespace core::app {
Context::Context(std::uint8_t pool_size) : pool_{pool_size} {}

utility::ThreadPool &Context::GetPool() noexcept { return pool_; }

void Context::SetSession(const proto::SessionInfo &info) {
  auto [lock, session] = session_.GetExclusive();
  session = info;
}
[[nodiscard]] const proto::SessionInfo &Context::Session() const noexcept {
  auto [lock, session] = session_.Get();
  return session;
}
}  // namespace core::app

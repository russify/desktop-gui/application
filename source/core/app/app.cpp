// qt
#include <qguiapplication.h>
#include <qqmlapplicationengine.h>
#include <qqmlcontext.h>

// clang format-off
#include "core/pch.hpp"
// clang-format on

// core
#include "core/app/app.hpp"

// ui
#include "ui/authetication/auth-client.hpp"
#include "ui/tracks/track-info-client.hpp"
#include "ui/user/location-storage.hpp"

namespace core::app {
Application::ReturnCode Application::Run(int argc, char **argv) {
// hiding console in Release config
#if !DEBUG
#if WINDOWS
  ::ShowWindow(::GetConsoleWindow(), SW_HIDE);
#endif  // WINDOWS
#endif  // !DEBUG

  QGuiApplication app{argc, argv};
  QQmlApplicationEngine engine;
  QUrl url{u"qrc:russify/qml/index.qml"_qs};
  QObject::connect(
      &engine, &QQmlApplicationEngine::objectCreated, &app,
      [url](QObject *obj, const QUrl &obj_url) {
        if (!obj && url == obj_url) {
          QCoreApplication::exit(ReturnCode::kFail);
        }
      },
      Qt::QueuedConnection);

  ui::player::Player player{&context_};
  engine.rootContext()->setContextProperty("player", &player);
  qmlRegisterSingletonType(QUrl{u"qrc:russify/qml/components/utility/Constants.qml"_qs}, "ConstantsPackage", 1, 0,
                           "Constants");

  ui::tracks::TrackInfoClient tracks_client;
  engine.rootContext()->setContextProperty("track_info_client", &tracks_client);

  ui::authentication::AuthClient auth_client{&context_};
  engine.rootContext()->setContextProperty("auth_client", &auth_client);

  ui::user::LocationStorage location_storage;
  engine.rootContext()->setContextProperty("location_storage", &location_storage);

  engine.load(url);

  return app.exec() == 0 ? ReturnCode::kSuccess : ReturnCode::kFail;
}
}  // namespace core::app

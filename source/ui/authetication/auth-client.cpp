// clang-format off
#include "core/pch.hpp"
// clang-format on

#include "ui/authetication/auth-client.hpp"

#include "core/utility/qt-helpers.hpp"

// nlohmann
#include <nlohmann/json.hpp>

namespace ui::authentication {
AuthClient::AuthClient(core::app::Context *context) noexcept : QObject(nullptr), client_{context} {}

void AuthClient::Authenticate(const QString &username, const QString &password) noexcept {
  try {
    client_.Authenticate(core::utility::ToStdString(username), core::utility::ToStdString(password));

    emit success();
  } catch (const std::exception &) {
    emit failure();
  }
}

bool AuthClient::IsAuthenticated() const noexcept { return client_.IsAuthenticated() && !client_.IsSessionExpired(); }

void AuthClient::Register(const QString &data) {
  try {
    proto::UserRegistrationInfo info;
    info.FromJson(nlohmann::json::parse(core::utility::ToStdString(data)));
    client_.Register(info);
    emit registrationSucceed();
  } catch (const std::exception &) {
    emit registrationFailed();
  }
}
}  // namespace ui::authentication

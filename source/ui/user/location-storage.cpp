#include "ui/user/location-storage.hpp"

// project
#include "core/utility/qt-helpers.hpp"

namespace ui::user {
namespace {
template <class ProtoMessage>
[[nodiscard]] QString JsonFromVector(const ProtoMessage &msg, bool add_comma) {
  auto json{msg.ToJson().dump()};
  if (add_comma) {
    json += ',';
  }

  return QString::fromStdString(json);
}

template <class T, class Serializer>
QString Serialize(const core::utility::Vector<T> &data, Serializer serializer, const char *label) {
  QString result;
  result = "{\"";
  result += label;
  result += "\": [";

  const auto size{data.size()};
  if (size > 0) {
    for (std::size_t index{}; index < size - 1; ++index) {
      result.append(serializer(data[index], true));
    }
    result.append(serializer(data[size - 1], false));
  }
  result += "]}";

  return result;
}
}  // namespace

QString LocationStorage::GetCountries() {
  return Serialize(client_.GetCountries(), &JsonFromVector<proto::Country>, "countries");
}

QString LocationStorage::GetLocations(const QString &country) {
  return Serialize(client_.GetLocations(core::utility::ToStdString(country)), &JsonFromVector<proto::Location>,
                   "locations");
}
}  // namespace ui::user

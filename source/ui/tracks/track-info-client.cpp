#include "ui/tracks/track-info-client.hpp"

namespace ui::tracks {
void TrackInfoClient::GetTracks(std::size_t limit, std::size_t offset) {
  auto json{internal_client_.GetTracks(limit, offset)};
  trackInfoArrived(QString::fromUtf8(json.data(), json.size()));
}
}  // namespace ui::tracks

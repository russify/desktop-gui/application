#include "ui/player/player.hpp"

// qt
#include <qaudiooutput.h>

// current project
#include <common/include/utility/memory/allocator.hpp>

namespace ui::player {
namespace {
class TaskCompletionGuard {
 public:
  explicit TaskCompletionGuard(boost::atomic_flag *flag) : flag_{flag} {}
  ~TaskCompletionGuard() {
    flag_->clear(boost::memory_order_release);
    flag_->notify_all();
  }

 private:
  boost::atomic_flag *flag_;
};
}  // namespace

Player::Player(core::app::Context *context)
    : ctx_{context}, strand_{ctx_->GetPool().GetIO()}, client_{&ctx_->GetPool().GetIO(), 3} {
  data_.open(QIODevice::ReadWrite);

  utility::memory::Allocator<QAudioOutput> alloc;
  auto output{alloc.allocate(1)};
  new (output) QAudioOutput;
  output->setVolume(50);
  player_.setAudioOutput(output);

  QObject::connect(&player_, &QMediaPlayer::positionChanged, [this](quint64 position) {
    playbackPositionChanged(offset_ + position);
    last_pos_ = position;
  });
}

void Player::StartFrom(std::uint16_t start_position) {
  force_stop_.store(true, boost::memory_order_release);
  strand_.post([start_position, this]() { StartFromImpl(start_position); });
}

void Player::Pause() { player_.pause(); }

void Player::Resume() { player_.play(); }

void Player::ChangeVolume(std::uint8_t volume) { player_.audioOutput()->setVolume(volume); }

void Player::CheckPlayerState() {
  if (player_.mediaStatus() != QMediaPlayer::EndOfMedia) {
    return;
  }

  if (length_ * 1000 - player_.position() + offset_ > 100) {  // qt tick rate
    player_.setSourceDevice(&data_);
    player_.setPosition(last_pos_);
    player_.play();
  }
}

void Player::SetCurrentTrack(std::size_t track_id, std::size_t length) noexcept {
  current_track_id_ = track_id;
  length_ = length;
}

void Player::StartFromImpl(std::uint16_t start_position) {
  bool need_restart{player_.playbackState() == QMediaPlayer::PlayingState};
  player_.stop();

  AwaitDownloadTask();
  is_task_active_.test_and_set(boost::memory_order_release);  // we have active task from some thread here

  try {
    ctx_->GetPool().PushTask([this, start_position, need_restart]() mutable {
      try {
        client_.Connect();
      } catch (const std::exception &ex) {
        emit fail(ex.what());
        return;
      }

      TaskCompletionGuard guard{&is_task_active_};  // clear on task end
      if (force_stop_.load(boost::memory_order_acquire)) {
        return;
      }

      offset_ = start_position * 1000;  // seconds to millis

      try {
        startDownloading();
        start_position = ReadSinglePacket(current_track_id_, start_position);
      } catch (const std::exception &ex) {
        emit fail(ex.what());
        return;
      }

      player_.setSourceDevice(&data_);  // setPosition will work only after this call
      if (need_restart) {
        player_.play();
      }

      DownloadPackets(start_position);
    });
  } catch (const std::exception &ex) {
    emit fail(ex.what());
    return;
  }
}

std::size_t Player::ReadSinglePacket(std::size_t track_id, std::size_t start_position) {
  const auto end{std::min<std::uint16_t>(start_position + max_packet_size, length_)};
  auto packet{client_.NextPacket(track_id, start_position, end)};
  data_.buffer().append(packet.data(), packet.size());
  newDataLoaded(end);

  return end;
}

void Player::AwaitDownloadTask() {
  // prevents other tasks from executing until the last task is completed
  static constexpr auto max_active_time{std::chrono::milliseconds{100}};
  const auto begin{boost::chrono::steady_clock::now()};
  while (true) {
    if (!is_task_active_.test(boost::memory_order_acquire)) {
      break;
    }

    if (boost::chrono::steady_clock::now().time_since_epoch().count() <=
        begin.time_since_epoch().count() + max_active_time.count()) {
      break;
    }
  }

  if (is_task_active_.test(boost::memory_order_acquire)) {
    is_task_active_.wait(true, boost::memory_order_acquire);
  }

  data_.buffer().clear();
  force_stop_.store(false, boost::memory_order_relaxed);
}

void Player::DownloadPackets(std::uint16_t start_position) {
  static constinit std::uint8_t max_retry_count{3};

  std::uint8_t retry_count{};
  while (start_position != length_ && !force_stop_.load(boost::memory_order_acquire) &&
         !ctx_->GetPool().GetIO().stopped()) {
    try {
      start_position = ReadSinglePacket(current_track_id_, start_position);
      CheckPlayerState();
      retry_count = 0;
    } catch (const std::exception &ex) {
      if (retry_count++ == max_retry_count) {
        emit fail(ex.what());
        return;
      }
    }
  }
  // loaded_.store(start_position == length, boost::memory_order_release);
}
}  // namespace ui::player

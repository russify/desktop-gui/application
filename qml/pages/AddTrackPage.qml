import QtQuick
import QtCore
import QtQuick.Dialogs
import QtQuick.Controls

import ConstantsPackage

import "../components/utility"

DefaultWindow {
    id: root

    RoundShape {
        height: 700
        width: 500

        anchors {
            centerIn: parent
        }

        color: Constants.white
        radius: Constants.windowRadius

        DefaultText {
            id: addLabel
            anchors {
                horizontalCenter: parent.horizontalCenter
                top: parent.top

                topMargin: 35
            }

            text: "<h1>NEW TRACK</h1>"
        }

        Column {
            spacing: 30

            anchors {
                top: addLabel.bottom
                left: parent.left
                right: parent.right

                topMargin: 20
                leftMargin: 20
                rightMargin: 20
            }

            Row {
                spacing: 50
                DefaultText {
                    text: "Name"
                    width: 100

                    anchors {
                        verticalCenter: parent.verticalCenter
                    }
                } // DefaultText

                ValidatedTextField {
                    width: 300
                    height: 30

                    background: Rectangle {
                        radius: Constants.windowRadius
                        color: Constants.white
                    } // Rectangle

                    placeholderText: "Track Name"

                    validator: RegularExpressionValidator {
                        regularExpression: /^.{2,256}?$/
                    }

                    errorMessage: "Track name here"
                    defaultState: true
                } // ValidatedTextField
            } // Row
            
            Row {
                spacing: 50

                DefaultText {
                    text: "Image"
                    width: 100

                    anchors {
                        verticalCenter: parent.verticalCenter
                    }
                } // DefaultText

                DefaultButton {
                    text: "<b>Select file</b>"

                    onClicked: () => {
                        fileDialog.open()
                    }
                }

                FileDialog {
                    property string image

                    id: fileDialog
                    currentFolder: StandardPaths.standardLocations(StandardPaths.PicturesLocation)[0]
                    onAccepted: image = selectedFile
                } // FileDialog
            } // Row

            Row {
                spacing: 50

                DefaultText {
                    text: "Track data"
                    width: 100

                    anchors {
                        verticalCenter: parent.verticalCenter
                    }
                } // DefaultText

                DefaultButton {
                    text: "<b>Select file</b>"

                    onClicked: () => {
                        trackFileDialog.open()
                    }
                }

                FileDialog {
                    property string image

                    id: trackFileDialog
                    currentFolder: StandardPaths.standardLocations(StandardPaths.PicturesLocation)[0]
                    onAccepted: image = selectedFile
                } // FileDialog
            } // Row

            Row {
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
                DefaultText {
                    text: "<b>OR</b>"
                }
            } // Row

            Row {
                spacing: 50

                DefaultText {
                    text: "Track data"
                    width: 100

                    anchors {
                        verticalCenter: parent.verticalCenter
                    }
                } // DefaultText

                ValidatedTextField {
                    width: 300
                    height: 30

                    background: Rectangle {
                        radius: Constants.windowRadius
                        color: Constants.white
                    } // Rectangle

                    placeholderText: "Track Url"

                    validator: RegularExpressionValidator {
                        regularExpression: /^.{2,256}?$/
                    }

                    errorMessage: "Track Url Here"
                    defaultState: true
                }// ValidatedTextField
            } // Row

            Row {
                spacing: 50

                DefaultText {
                    width: 100

                    text: "Album"
                }

                DefaultComboBox {
                    width: 300
                    height: 30
                    model: ["Acustic", "Post-modern"]
                } // DefaultComboBox
            } // Row

            Row {
                spacing: 50

                DefaultText {
                    width: 100

                    text: "Year"
                } // DefaultText

                DefaultComboBox {
                    width: 70
                    height: 30
                    model: generateYears()

                    function generateYears() {
                        const result = [];
                        const maxYear = parseInt(Qt.formatDate(new Date(), "yyyy"));
                        for (let index = maxYear; index >= 1700; index -= 1) {
                            result.push(index);
                        }
                        return result;
                    }
                } // DefaultComboBox
            } // Row
        } // Column

        Row {
            spacing: 20
            anchors {
                bottom: parent.bottom
                horizontalCenter: parent.horizontalCenter

                bottomMargin: 50
            }

            DefaultButton {
                text: "<b>Back</b>"
                width: 100
                height: 30
            } // DefaultButton

            DefaultButton {
                text: "<b>Confirm</b>"
                width: 100
                height: 30
            } // DefaultButton
        } // Row
    } // RoundShape
} // DefaultWindow
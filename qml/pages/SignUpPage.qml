import QtQuick
import QtQuick.Controls
import ConstantsPackage

import "../components/utility"

DefaultWindow {
    id: root

    Keys.onReturnPressed: (event) => {
        signup();
    }

    Keys.onEnterPressed: () => {
        signup();
    }

    function signup() {
        if (!nameInput.acceptableInput) {
            nameInput.showError(null, false);
            return;
        }

        if (!emailInput.acceptableInput) {
            emailInput.showError(null, false);
            return;
        }

        if (!phoneInput.acceptableInput) {
            phoneInput.showError(null, false);
            return;
        }

        if (!passwordInput.acceptableInput) {
            passwordInput.showError(null, false);
            return;
        }

        auth_client.Register(`{"name": "${nameInput.text}", "email": "${emailInput.text}", "phone": ${parseInt(phoneInput.text)},
        "password": "${passwordInput.text}", "country": {"name": "${countryBox.currentText}"}, "location": {"name": "${locationBox.currentText}"}}`);
    }

    RoundShape {
        id: controls
        height: 700
        width: 500

        anchors {
            centerIn: parent
        }

        color: Constants.white
        radius: Constants.windowRadius

        DefaultText {
            id: regLabel
            anchors {
                horizontalCenter: parent.horizontalCenter
                top: parent.top

                topMargin: 35
            }

            text: "<h1>REGISTRATION</h1>"
        }

        Column {
            id: body
            spacing: 30

            anchors {
                top: regLabel.bottom
                left: parent.left
                right: parent.right

                topMargin: 20
                leftMargin: 20
                rightMargin: 20
            }

            Row {
                spacing: 50
                anchors {
                    left: parent.left
                    right: parent.right
                }

                DefaultText {
                    id: nameLabel
                    text: "Name"
                    width: 100

                    anchors {
                        verticalCenter: parent.verticalCenter
                    }
                } // DefaulText

                ValidatedTextField {
                    id: nameInput
                    width: 300
                    height: 30

                    background: Rectangle {
                        radius: Constants.windowRadius
                        color: Constants.white
                    } // Rectangle

                    placeholderText: "Your Full Name"
                    validator: RegularExpressionValidator {
                        regularExpression: /^([\pL]{2,64}) ([\pL]{2,64})( [\pL]{2,64})?$/
                    }

                    errorMessage: "You must enter you name and lastname (and patronimyc if you wish)"
                    defaultState: true

                    Keys.onReturnPressed: () => {
                        emailInput.focus = true;
                    }
                } // ValidatedTextField
            } // Row

            Row {
                spacing: 50
                anchors {
                    left: parent.left
                    right: parent.right
                }

                DefaultText {
                    id: emailLabel
                    text: "Email"
                    width: nameLabel.width

                    anchors {
                        verticalCenter: parent.verticalCenter
                    }
                } // DefaulText

                ValidatedTextField {
                    id: emailInput
                    width: nameInput.width
                    height: nameInput.height

                    background: Rectangle {
                        radius: Constants.windowRadius
                        color: Constants.white
                    } // Rectangle

                    placeholderText: "Email (will be your username)"
                    validator: RegularExpressionValidator {
                        regularExpression: /(?:[\pL0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[\pL0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[\pL0-9](?:[\pL0-9-]*[\pL0-9])?\.)+[\pL0-9](?:[\pL0-9-]*[\pL0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[\pL0-9-]*[\pL0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
                    }

                    errorMessage: "Invalid email"
                    defaultState: true

                    Keys.onReturnPressed: () => {
                        phoneInput.focus = true;
                    }
                } // ValidatedTextField
            } // Row

            Row {
                spacing: 50
                anchors {
                    left: parent.left
                    right: parent.right
                }

                DefaultText {
                    id: phoneLabel
                    text: "Phone"
                    width: nameLabel.width

                    anchors {
                        verticalCenter: parent.verticalCenter
                    }
                } // DefaulText

                ValidatedTextField {
                    id: phoneInput
                    width: nameInput.width
                    height: nameInput.height

                    background: Rectangle {
                        radius: Constants.windowRadius
                        color: Constants.white
                    } // Rectangle

                    validator: RegularExpressionValidator {
                        regularExpression: /^[0-9]{10,10}$/
                    }

                    errorMessage: "Invalid email"
                    defaultState: true

                    Keys.onReturnPressed: () => {
                        passwordInput.focus = true;
                    }
                } // ValidatedTextField
            } // Row

            Row {
                spacing: 50
                anchors {
                    left: parent.left
                    right: parent.right
                }

                DefaultText {
                    id: passwordLabel
                    text: "Password"
                    width: nameLabel.width

                    anchors {
                        verticalCenter: parent.verticalCenter
                    }
                } // DefaulText

                ValidatedTextField {
                    id: passwordInput
                    width: nameInput.width
                    height: nameInput.height

                    background: Rectangle {
                        radius: Constants.windowRadius
                        color: Constants.white
                    } // Rectangle

                    validator: RegularExpressionValidator {
                        regularExpression: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/
                    }

                    errorMessage: "Invalid password: must contain at least one upper case letter, one lower case letter, one number and one special character"
                    defaultState: true

                    Keys.onReturnPressed: () => {
                        countryBox.focus = true;
                    }

                    echoMode: TextInput.Password
                } // ValidatedTextField
            } // Row

            Row {
                spacing: 50
                anchors {
                    left: parent.left
                    right: parent.right
                }

                DefaultText {
                    id: countryLabel
                    text: "Country"
                    width: nameLabel.width

                    anchors {
                        verticalCenter: parent.verticalCenter
                    }
                } // DefaulText

                DefaultComboBox {
                    id: countryBox
                    width: nameInput.width
                    height: nameInput.height

                    model: location_storage ? JSON.parse(location_storage.GetCountries()).countries : null
                    itemTokenizer: function(item) {
                        return item.name
                    }

                    Keys.onReturnPressed: () => {
                        countryBox.currentIndex = highlightedIndex;
                        locationBox.focus = true;
                    }
                } // DefaultComboBox
            } // Row

            Row {
                spacing: 50
                anchors {
                    left: parent.left
                    right: parent.right
                }

                DefaultText {
                    id: locationLabel
                    text: "Location"
                    width: nameLabel.width

                    anchors {
                        verticalCenter: parent.verticalCenter
                    }
                } // DefaulText

                DefaultComboBox {
                    width: nameInput.width
                    height: nameInput.height

                    id: locationBox
                    model: location_storage ? JSON.parse(location_storage.GetLocations(countryBox.model[countryBox.currentIndex].name)).locations : null
                    itemTokenizer: function(item) {
                        return item.name;
                    }
                } // DefaultComboBox
            } // Row

            Row {
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }

                Column {
                    spacing: 20
                    DefaultText {
                        text: "<b>Or Sign Up Using</b>"
                    } // DefaultText

                    Row {
                        spacing: 20
                        SelectableImage {
                            id: gmailAuth
                            height: 40
                            width: height

                            image {
                                source: "qrc:///images/images/login/gmail-login.png"
                            }
                        } // SelectaleImage

                        SelectableImage {
                            height: gmailAuth.height
                            width: height

                            image {
                                source: "qrc:///images/images/login/vk-login.png"
                            }
                        } // SelectaleImage
                    } // Row
                } // Column
            } // Row
        } // Column

        Row {
            spacing: 20
            anchors {
                bottom: controls.bottom
                horizontalCenter: parent.horizontalCenter
                
                bottomMargin: 20
            }

            DefaultButton {
                id: okButton

                text: "<b>Back</b>"

                width: 100
                height: 30

                onClicked: () => {
                    stack.pop();
                }
            }

            DefaultButton {
                width: okButton.width
                height: okButton.height

                text: "<b>Confirm</b>"

                onClicked: () => {
                    signup();
                }
            } // DefaultButton
        } // Row
    } // RoundShape

    Connections {
        target: auth_client
        function onRegistrationSucceed() {
            stack.pop(null);
        }

        function onRegistrationFailed() {
            nameInput.showError("Request failed. Check your data");
        }
    }
} // DefaultWindow
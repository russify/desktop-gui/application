import QtQuick

import ConstantsPackage

import "../components/utility"

DefaultWindow {
    id: root
    readonly property int offset: 35

    Keys.onReturnPressed: (event) => {
        authenticate();
    }

    Keys.onEnterPressed: () => {
        authenticate();
    }

    function authenticate() {
        if (!usernameInput.acceptableInput) {
            usernameInput.showError(null, false);
            return;
        }

        if (!passwordInput.acceptableInput) {
            passwordInput.showError(null, false);
            return;
        }

        auth_client.Authenticate(usernameInput.text, passwordInput.text);
    }

    RoundShape {
        color: Constants.white
        radius: Constants.windowRadius
        
        width: 400
        height: 600

        anchors {
            centerIn: parent
        }

        DefaultText {
            id: login

            text: "<h1>Login</h1>"
            anchors {
                horizontalCenter: parent.horizontalCenter
                top: parent.top

                topMargin: 50
            }
        } // DefaultText

        Item {
            anchors {
                left: parent.left
                right: parent.right
                top: login.bottom

                leftMargin: offset
                rightMargin: offset
                topMargin: offset
            }
            
            DefaultText {
                id: usernameLabel
                font.pixelSize: 10
                text: "Username"
            } // DefaultText

            Item {
                id: username
                height: usernameInput.height

                anchors {
                    top: usernameLabel.bottom
                    left: parent.left
                    right: parent.right

                    topMargin: 1
                }

                Image {
                    id: usernameImage

                    height: usernameInput.height
                    width: height
                    mipmap: true
                    source: "qrc:///images/images/login/user.svg"
                } // Image

                TextFieldWithBottomLine {
                    id: usernameInput
                    anchors {
                        left: usernameImage.right
                        right: parent.right
                        
                        leftMargin: 3
                    }

                    placeholderText: "Type your username"
                    validator: RegularExpressionValidator {
                        regularExpression: /^[\S]{6,128}$/
                    }
                    defaultState: true
                    errorMessage: "Username must contain at least 6 symbols (at most 128)"
                } // TextFieldWithBottomLine
            } // Item

            DefaultText {
                id: passwordLabel
                font.pixelSize: 10
                text: "Password"
                anchors {
                    top: username.bottom

                    topMargin: 15
                }
            } // DefaulText

            Item {
                id: password
                height: passwordInput.height

                anchors {
                    top: passwordLabel.bottom
                    left: parent.left
                    right: parent.right

                    topMargin: 1
                }

                Image {
                    id: passwordImage

                    height: passwordInput.height
                    width: height
                    mipmap: true
                    source: "qrc:///images/images/login/password-lock.svg"
                } // Image

                TextFieldWithBottomLine {
                    id: passwordInput
                    defaultState: true

                    anchors {
                        left: passwordImage.right
                        right: parent.right
                        
                        leftMargin: 3
                    }

                    placeholderText: "Type your password"

                    echoMode: TextInput.Password

                    validator: RegularExpressionValidator {
                        regularExpression: /^[\S]{8,32}$/
                    }
                    errorMessage: "Password must contain at least 8 symbols (at most 32)"
                } // TextFieldWithBottomLine
            } // Item

            HoveredLabel {
                id: resetPassword
                
                anchors {
                    right: parent.right
                    top: password.bottom

                    topMargin: 12
                }

                mainColor: Constants.white
                text: "Forgot password?"
                font.pixelSize: 10
            } // HoveredLabel

            RoundShape {
                id: loginButton

                radius: 15
                
                height: 30

                anchors {
                    top: resetPassword.bottom
                    left: parent.left
                    right: parent.right

                    leftMargin: offset
                    rightMargin: offset
                    topMargin: offset
                }

                gradient_stops: [[0.0, "#63D5E3"], [1.0, "#E93AF5"]]
                gradient_begin: Qt.point(0, 0)
                gradient_end: Qt.point(width, 0)
                use_gradient: true

                DefaultText {
                    id: loginText
                    
                    anchors {
                        centerIn: parent
                    }

                    text: "<b>LOGIN</b>"
                } // DefaultText

                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true
                    propagateComposedEvents: true

                    onClicked: function() {
                        root.authenticate();
                    }

                    onPressed: function() {
                        loginText.color = Constants.hoveredTextColor;
                    }

                    onReleased: function() {
                        loginText.color = Constants.fontColor;
                    }
                } // MouseArea

                Connections {
                    target: auth_client
                    function onSuccess() {
                        usernameInput.hideError(true);
                        stack.pop()
                    }

                    function onFailure() {
                        usernameInput.showError("Invalid username or password", true);
                    }
                } // Connections
            } // RoundShape

            DefaultText {
                id: loginMethodsLabel

                text: "Or Login Using"

                anchors {
                    top: loginButton.bottom
                    horizontalCenter: parent.horizontalCenter
                    
                    topMargin: 35
                }
            } // DefaultText

            Row {
                id: loginMethods

                anchors {
                    top: loginMethodsLabel.bottom
                    horizontalCenter: parent.horizontalCenter

                    topMargin: 15
                }

                readonly property int imageSize: 40

                spacing: 5
                    
                SelectableImage {
                    height: loginMethods.imageSize
                    width: loginMethods.imageSize

                    image {
                        source: "qrc:///images/images/login/gmail-login.png"
                    }
                } // SelectableImage

                SelectableImage {
                    height: loginMethods.imageSize
                    width: loginMethods.imageSize
                    image {
                        source: "qrc:///images/images/login/vk-login.png"
                    }
                } // SelectableImage
            } // Row
        } // Item

        HoveredLabel {
            anchors {
                bottom: parent.bottom
                horizontalCenter: parent.horizontalCenter

                bottomMargin: 20
            }
            mainColor: Constants.white
            text: "<h2>Sign Up<h2>"

            MouseArea {
                anchors.fill: parent

                onClicked: () => {
                    let component = Qt.createComponent('SignUpPage.qml');
                    stack.push(component.createObject(root, {root: root, tray: tray}));
                }
            }
        } // DefaultText
    } // RoundShape

    Component.onCompleted: () => {
        if (auth_client.IsAuthenticated()) {
            stack.pop();
        }
    }
} // DefaultWindow
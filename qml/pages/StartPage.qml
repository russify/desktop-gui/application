import QtQuick  
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Window 2.2

import ConstantsPackage

import "../components/play-bar"
import "../components/nav-bar"
import "../components/music-list"
import "../components/utility"

DefaultWindow {
    NavBar {
        id: navbar

        color: "transparent"
        radius: parent.radius

        anchors {
            top: menu_bar.bottom
            left: parent.left
            right: parent.right

            leftMargin: parent.width * 0.03
            rightMargin: parent.width * 0.03
            topMargin: parent.height * 0.05
        }

        height: parent.height * 0.1
    }

    MusicList {
        color: Constants.white

        radius: parent.radius
        anchors {
            left: parent.left
            right: parent.right
            top: navbar.bottom

            leftMargin: parent.width * 0.03
            rightMargin: parent.width * 0.03
            topMargin: parent.height * 0.05
        }

        height: parent.height * 0.6
    }

    PlayBar {
        color: Qt.rgba(1, 1, 1, 0.2)

        top_left_radius: parent.radius
        top_right_radius: parent.radius

        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom

            leftMargin: parent.width * 0.03
            rightMargin: parent.width * 0.03
        }

        height: parent.height * 0.08

        // TODO(shmuk): delete this section
        track_source: "https://static.wikia.nocookie.net/hunterxhunter/images/a/ae/Opening_1.png/revision/latest/scale-to-width-down/170?cb=20221218223755"
            
        track_name: "Taiyou Wa Yoru Mo Kagayaku by Wino"
        group_name: "HunterXHunter OST"

        duration: 241

        Component.onCompleted: function() {
            player.SetCurrentTrack(1, 241);
            player.StartFrom(0);
        }

        Resizable {
            resizable: root
            bottom_side: true
        }
    }
}
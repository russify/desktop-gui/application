import QtQuick  
import Qt.labs.platform
import QtQuick.Controls

import "pages"

Window {
    id: root
    color: "transparent"
    flags: Qt.FramelessWindowHint | Qt.Window

    visible: true
    title: qsTr("RUssify")

    minimumWidth: 750
    minimumHeight: 850    

    SystemTrayIcon {
        id: tray
        visible: false
        icon.source: "qrc:///images/images/main-menu-bar/cancel.svg"

        onActivated: function() {
            root.flags = Qt.Window | Qt.FramelessWindowHint
            root.show()
            root.raise()
            root.requestActivate()
            hide()
        }
    }

    StackView {
        id: stack
        anchors.fill: parent

        initialItem: start 
    }

    StartPage {   
        id: start

        root: root
        tray: tray
    }

    Component.onCompleted: function() {
        if (!auth_client.IsAuthenticated()) {
            let component = Qt.createComponent('pages/LoginPage.qml');
            stack.push(component.createObject(root, {root: root, tray: tray}));
        }
    }
}
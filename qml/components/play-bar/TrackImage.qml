import QtQuick
import Qt5Compat.GraphicalEffects

import "../utility"

Image {
    id: track_img
    mipmap: true

    anchors {
        verticalCenter: parent.verticalCenter
        left: parent.left
        leftMargin: parent.width * 0.05
    }
            
    property bool rounded: true
    property bool adapt: true
    property int radius: 35

    width: radius
    height: radius

    layer {
        enabled: rounded
        effect: OpacityMask {
            maskSource: Item {
                width: track_img.width
                height: track_img.height

                Rectangle {
                    anchors.centerIn: parent
                    width: track_img.adapt ? track_img.width : Math.min(track_img.width, track_img.height)
                    height: track_img.adapt ? track_img.height : Math.min(track_img.width, track_img.height)
                    radius: Math.min(width, height)
                }
            }
        }
    }

    RoundMouseArea {
        anchors.fill: parent
        accepted_buttons: Qt.LeftButton

        onRoundPressed: function(event) {
            console.log(event)
        }
    }
}
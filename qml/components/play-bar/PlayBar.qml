import QtQuick
import QtQuick.Controls

import "../utility"
import "../../js/text-helpers.js" as TextHelpers

RoundShape {
    id: bar

    property point content_start 
    property alias track_source: track_img.source
    property alias track_img_radius: track_img.radius

    property alias track_name: track_info.track_name
    property alias group_name: track_info.group_name

    property alias duration: playback_slider.to

    MusicSlider {
        id: playback_slider

        from: 0
    }

    TrackImage {
        id: track_img
    }

    TrackNameGroupLabel {
        id: track_info

        max_track_name_length: 21
        max_group_name_length: 32

        // yeah, yeah, hardcode
        width: Math.max(100, parent.width * 0.1)
        anchors {
            left: track_img.right
            top: playback_slider.bottom
            bottom: parent.bottom

            leftMargin: parent.width * 0.03
        }
    }

    PlayControlButtons {
        id: controls_bar
            
        x: parent.width * 0.5
        width: 0.3 * parent.width

        anchors {
            top: playback_slider.bottom
            bottom: parent.bottom
            left: track_info.right

            leftMargin: parent.width * 0.1
        }

        onPlayingStateChanged: function(playing) {
            playback_slider.move_on_click = !playing;
        }
    }

    DefaultText {
        id: duration_label 
        anchors {
            top: playback_slider.bottom
            right: playback_slider.right

            topMargin: parent.height * 0.05
        }

        text: TextHelpers.formatDuration(bar.duration, true)
    }
}
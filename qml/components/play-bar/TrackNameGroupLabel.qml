import QtQuick
import QtQuick.Controls

import "../utility"
import "../../js/text-helpers.js" as TextHelpers

Rectangle {
    required property string track_name
    required property string group_name

    property int max_track_name_length
    property int max_group_name_length
    property bool play_animation

    color: "transparent"

    MouseArea {
        anchors.fill: parent
        propagateComposedEvents: true
        hoverEnabled: true

        DefaultText {
            id: track_name_label

            text: TextHelpers.formatTextWithMaxLength(track_name, max_track_name_length)

            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
                topMargin: parent.height * 0.3
            }
    
            MouseArea {
                anchors.fill: parent
                hoverEnabled: true

                onPressed: function() {
                    console.log("Track name pressed");
                }

                onEntered: function() {
                    track_name_label.font.underline = true;
                }

                onExited: function() {
                    track_name_label.font.underline = false;
                }
            }
        }

        DefaultText  {
            id: group_name_label
            font.pixelSize: track_name_label.font.pixelSize * 0.75

            text: TextHelpers.formatTextWithMaxLength(group_name, max_group_name_length)

            wrapMode: track_name_label.wrapMode

            anchors {
                left: parent.left
                right: parent.right
                top: track_name_label.bottom
                topMargin: 0.05 * parent.height
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true

                onPressed: function() {
                    console.log("Group name pressed");
                }

                onEntered: function() {
                    group_name_label.font.underline = true;
                }

                onExited: function() {
                    group_name_label.font.underline = false;
                }
            }
        }
    }
}
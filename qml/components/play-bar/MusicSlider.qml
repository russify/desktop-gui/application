import QtQuick
import QtQuick.Controls

import ConstantsPackage

import "../utility"
import "../../js/text-helpers.js" as TextHelpers

Slider {
    id: playback_slider
    height: 7
    anchors {
        top: parent.top
        left: parent.left
        right: parent.right

        leftMargin: 0.05 * parent.width
        rightMargin: 0.05 * parent.width
    }
    live: false

    property bool is_pressed_: false;
    property bool move_on_click: true
    property int last_value_: 0

    background: Rectangle {
        anchors.fill: parent
        color: Constants.white

        Rectangle {
            id: download_bar
            color: "lightgray"

            anchors {
                top: parent.top
                bottom: parent.bottom
            }

            width: 0

            Connections {
                target: player
                function onNewDataLoaded(position) {
                    download_bar.width = playback_slider.width * (position / playback_slider.to) - download_bar.x;
                }
            }
        }
    }

    handle: Rectangle {
        id: slider_handle

        anchors.verticalCenter: parent.verticalCenter

        width: Math.min(parent.width, parent.height) + Math.min(parent.width, parent.height) / 2
        height: width
        radius: width

        x: -radius / 2

        color: "white"

        Connections {
            target: player
            function onPlaybackPositionChanged(pos) {
                if (!playback_slider.is_pressed_) { // pos in millis
                    slider_handle.x = playback_slider.width * ((pos / 1000) / playback_slider.to) - slider_handle.radius / 2;
                }
            }

            function onStartDownloading() {
                is_pressed_ = false;
            }
        }
    }

    Connections {
        target: player
        function onNewDataLoaded(position) {
            playback_slider.value = position;
        }
    }

    ToolTip {
        id: tip
        visible: playback_area.containsMouse

        contentItem: DefaultText {
            text: tip.text
        }

        background: Rectangle {
            color: "transparent"
        }
    }
    
    MouseArea {
        id: playback_area
        anchors.fill: parent
        hoverEnabled: true
        propagateComposedEvents: true

        onPositionChanged: function(event) {
            tip.text = TextHelpers.formatDuration((event.x / playback_slider.width * playback_slider.to) >> 0, true);
            tip.x = event.x - tip.width / 2;
        }

        onPressed: function(event) {
            let val = ((event.x / width) * to) >> 0;

            is_pressed_ = true;

            download_bar.width = 0;
            download_bar.x = width * (val / to);

            if (move_on_click) {
                slider_handle.x = download_bar.x - slider_handle.radius / 2;
            }

            player.StartFrom(val);

            event.accepted = false;
        }
    }

    onWidthChanged: function() {
        download_bar.width = width * (value / to) - download_bar.x;
    }
}
import QtQuick
import Qt5Compat.GraphicalEffects

import "../utility"

Rectangle {
    id: controls_bar
    property int imgs_radius: 50
    color: "transparent"

    signal playingStateChanged(playing: bool)

    MouseArea {
        anchors.fill: parent
        propagateComposedEvents: true
        hoverEnabled: true

        SelectableImage {
            id: prev_button
            radius: imgs_radius

            anchors {
                left: parent.left
                leftMargin: width < parent.width ? (parent.width - width * 3) / 2 : 0

                verticalCenter: parent.verticalCenter
            }

            image {
                source: "qrc:///images/images/play-bar/switch-button.svg"
                rotation: 180
            }
        }

        SelectableImage {
            id: play_button
            radius: imgs_radius

            anchors {
                left: prev_button.right

                verticalCenter: parent.verticalCenter
            }

            image {
                source: "qrc:///images/images/play-bar/play-button.svg"
            }

            property bool playing: false
            mouseArea {
                onRoundPressed: function() {
                    if (playing) {
                        player.Pause();
                        playing = false;
                        image.source = "qrc:///images/images/play-bar/play-button.svg"
                    } else {
                        player.Resume();
                        playing = true;
                        image.source = "qrc:///images/images/play-bar/pause.svg"
                    }

                    controls_bar.playingStateChanged(playing);
                }
            }
        }



        SelectableImage {
            id: next_button
            radius: imgs_radius

            anchors {
                left: play_button.right

                verticalCenter: parent.verticalCenter
            }

            image {
                source: "qrc:///images/images/play-bar/switch-button.svg"
            }
        }
    }
}
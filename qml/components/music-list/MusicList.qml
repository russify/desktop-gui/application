import QtQuick
import QtQuick.Controls

import "../utility"

Rectangle {
    Rectangle {
        id: first_row_

        color: "transparent"
        height: 35
        anchors {
            left: parent.left
            right: parent.right
        }

        DefaultText {
            id: name_
            text: "Name"

            width: Math.max(100, parent.width * 0.1)
            anchors {
                left: parent.left
                leftMargin: 35 + parent.width * 0.08
            }
        }

        DefaultText {
            id: album_
            text: "Album"

            width: Math.max(80, parent.width * 0.1)
            anchors {
                left: name_.right
                leftMargin: parent.width * 0.1
            }
        }

        DefaultText {
            id: year_

            anchors {
                left: album_.right
                leftMargin: parent.width * 0.1
            }

            text: "Year"
        }

        DefaultText {
            width: Math.max(80, parent.width * 0.1)
            anchors {
                right: parent.right
                rightMargin: parent.width * 0.1
            }

            text: "Duration"
        }
    }    

    ListView {
        id: track_list

        property int offset: 0
        property int limit: 50
        property bool append_bottom: true

        property int internal_offset: 0

        anchors {
            top: first_row_.bottom
            topMargin: 2

            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        spacing: 2

        ListModel {
            id: model_delegate_
        }

        Connections {
            target: track_info_client
            function onTrackInfoArrived(json) {
                if (model_delegate_.count == track_list.limit * 3) {
                    if (track_list.append_bottom) {
                        track_list.internal_offset += track_list.limit;
                        model_delegate_.remove(0, track_list.limit);
                    } else {
                        model_delegate_.remove(track_list.limit * 2 - 1, track_list.limit);
                        track_list.internal_offset -= track_list.limit;
                    }
                }

                let data = JSON.parse(json);
                if (track_list.append_bottom) {
                    data["data"].forEach(object => model_delegate_.append(object));
                } else {
                    let tracks = data["data"];
                    for (let index = tracks.length - 1; index >= 0; --index) {
                        model_delegate_.insert(0, tracks[index]);
                    }
                    track_list.positionViewAtIndex(track_list.limit, ListView.Beginning);
                }
            }
        }

        clip: true

        onFlickEnded: function() {
            let item_height = 0;
            if (contentItem.children.length > 0) {
                item_height = contentItem.children[0].height;
            }

            let diff = contentHeight - contentHeight * visibleArea.yPosition - item_height * limit;
            if (diff <= contentHeight && track_list.internal_offset > 0) {
                track_list.append_bottom = false;
                track_list.offset -= track_list.limit;
                track_info_client.GetTracks(track_list.limit, track_list.offset);
            } else if(diff <= height) {
                track_list.append_bottom = true;
                track_list.offset += track_list.limit;
                track_info_client.GetTracks(track_list.limit, track_list.offset);
            }
        }

        delegate: ListItem {
            anchors {
                left: parent ? parent.left : undefined
                right: parent ? parent.right : undefined
            }
        }

        model: model_delegate_

        // TODO(shmuk): replace?
        Component.onCompleted: function() {
            track_info_client.GetTracks(limit * 2, offset);
        }
    }
}
import QtQuick

import "../utility"
import "../play-bar"
import "../../js/text-helpers.js" as TextHelpers

Item {
    property alias image_radius: track_img.radius

    height: image_radius

    TrackImage {
        id: track_img

        source: image_url
    }

    TrackNameGroupLabel {
        id: track_info

        width: Math.max(100, parent.width * 0.1)
        anchors {
            left: track_img.right
            leftMargin: parent.width * 0.03
        }

        track_name: TextHelpers.formatTextWithMaxLength(name, 22)
        group_name: TextHelpers.formatTextWithMaxLength(group, 27)
    }

    DefaultText {
        id: album_label

        width: Math.max(80, parent.width * 0.1)
        anchors {
            left: track_info.right
            leftMargin: parent.width * 0.1
        }
        
        text: TextHelpers.formatTextWithMaxLength(album, 20)
    }

    DefaultText {
        id: year_label

        anchors {
            left: album_label.right
            leftMargin: parent.width * 0.1
        }

        text: year
    }

    DefaultText {
        id: duration_label

        width: Math.max(80, parent.width * 0.1)
        anchors {
            right: parent.right
            rightMargin: parent.width * 0.1
        }

        text: TextHelpers.formatDuration(duration, true)
    }
}
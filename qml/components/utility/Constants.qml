pragma Singleton
import QtQuick

QtObject {
    readonly property color white: Qt.rgba(1, 1, 1, 0.35)

    readonly property int fontSize: 14
    readonly property color fontColor: "white"
    readonly property color hoveredTextColor: "lightgrey"

    readonly property int windowRadius: 10
}
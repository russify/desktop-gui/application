import QtQuick

import ConstantsPackage

RoundShape {
    property alias root : menu_bar.root
    property alias tray : menu_bar.system_tray
    property alias menu_bar: menu_bar
    property alias mouseArea: mouseArea

    MenuBarWithAppBorders {
        id: menu_bar

        radius: parent.radius
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        propagateComposedEvents: true
        z: -100

        onClicked: () => {
            main_window.focus = true;
        }

        Resizable {
            resizable: root
            bottom_side: true
        }

        Resizable {
            resizable: root
            right_side: true
        }

        Resizable {
            resizable: root
            left_side: true
        }
    }

    id: main_window

    width: parent.width
    height: parent.height

    radius: Constants.windowRadius
        
    gradient_stops: [[0.0, "#78579E"], [1.0, "#E64386"]]
    gradient_begin: Qt.point(0, 0)
    use_gradient: true
}
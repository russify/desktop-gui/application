import QtQuick

import "../main-menu-bar"

Item {
    required property variant root
    property alias system_tray: menu_bar.tray
    required property int radius

    MainMenuBar {
        id: menu_bar
            
        window: root

        width: root.width
        height: 30

        top_left_radius: parent.radius
        top_right_radius: parent.radius

        color: Qt.rgba(1, 1, 1, 0.2)

        icon_size: Qt.size(15, 15)
        button_size: Qt.size(40, 30)
        buttons_y: 0

        Resizable {
            resizable: root
            top_side: true
        }
    }
}
import QtQuick

MouseArea {
    property int accepted_buttons: Qt.LeftButton | Qt.RightButton

    signal roundClicked(variant event)
    signal roundDoubleClicked(variant event)
    signal roundPositionChanged(variant event)
    signal roundPressAndHold(variant event)
    signal roundPressed(variant event)
    signal roundReleased(variant event)
    signal roundWheel(variant event)
    signal roundEntered()
    signal roundExited()
    
    anchors.fill: parent
    hoverEnabled: true
    propagateComposedEvents: true
    acceptedButtons: accepted_buttons

    property bool entered: false

    function containsMouse(){
        var x1 = width / 2;
        var y1 = height / 2;
        var x2 = mouseX;
        var y2 = mouseY;
        var distanceFromCenter = Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2);
        var radiusSquared = Math.pow(Math.min(width, height) / 2, 2);
        return distanceFromCenter < radiusSquared;
    }

    onClicked: function(event) {
        if (containsMouse()) {
            roundClicked(event);
        }
    }

    onDoubleClicked: function(event) {
        if (containsMouse(event)) {
            roundDoubleClicked(event);
        }
    }

    onPositionChanged: function(event) {
        if (containsMouse(event)) {
            if (!entered) {
                entered = true;
                roundEntered();
            }
            roundPositionChanged(event);
            return;
        }

        onExited()
    }

    onPressAndHold: function(event) {
        if (containsMouse(event)) {
            roundPressAndHold(event);
        }
    }

    onPressed: function(event) {
        if (containsMouse()) {
            roundPressed(event);
        }
    }

    onReleased: function(event) {
        if (containsMouse()) {
            roundReleased(event);
        }
    }

    onWheel: function(event) {
        if (containsMouse()) {
            roundWheel(event);
        }
    }

    onExited: function() {
        if (entered) {
            entered = false;
            roundExited();
        }
    }
}
import QtQuick

Rectangle {
    id: root
    color: "transparent"
    radius: Math.min(parent.width, parent.height)
    width:  radius
    height: radius

    property alias image: image_
    property alias mouseArea: roundMouseArea

    border {
        color: "transparent"
        width: 1
    }

    Image {
        id: image_
        mipmap: true

        width: parent.width * 0.95
        height: width

        anchors {
            centerIn: parent
        }

        RoundMouseArea {
            id: roundMouseArea
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton

            onRoundEntered: function() {
                root.border.color = "white";
            }

            onRoundExited: function() {
                root.border.color = "transparent";
            }

            onRoundPressed: parent.onPressed
        }
    }
}
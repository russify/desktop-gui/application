import QtQuick
import QtQuick.Controls

ValidatedTextField {
    positionElement: line

    background: Rectangle {
        id: line
        color: "transparent"

        Rectangle {
            anchors {
                top: parent.bottom
                left: parent.left
                right: parent.right
            }

            height: 2

            color: Constants.white
        }
    }
}
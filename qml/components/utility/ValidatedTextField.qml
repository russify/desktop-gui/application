import QtQuick
import QtQuick.Controls

TextField {
    property alias errorMessage : error.text
    property bool defaultState : false
    property alias positionElement : error.positionElement

    color: Constants.fontColor
    font.pixelSize: Constants.fontSize

    QtObject {
        id: prevState
        property string lastError
    }

    DefaultText {
        id: error
        property variant positionElement: parent
        property bool force
        anchors {
            top: positionElement.bottom
            
            topMargin: 1
        }

        font.pixelSize: 10
        color: "red"
        opacity: parent.defaultState ? 0.0 : 1.0
        wrapMode: Text.WordWrap
        width: parent.width
    }

    onTextEdited: () => {
        error.opacity = acceptableInput || defaultState || error.force ? 0.0 : 1.0
    }

    function showError(msg, force) {
        if (msg) {
            prevState.lastError = errorMessage;
            errorMessage = msg;
        }
        error.opacity = 1.0;
        error.force = force === null ? true : force;
    }

    function hideError(restoreMessage) {
        error.force = false;
        error.opacity = 0.0;
        if (restoreMessage) {
            errorMessage = prevState.lastError;
            prevState.lastError = null;
        }
    }

    onActiveFocusChanged: function() {
        if (!activeFocus) {
            defaultState = false;
        } else if (!defaultState) {
            onTextEdited();
        }
    }
}
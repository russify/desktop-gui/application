import QtQuick

MouseArea {
    property variant resizable
    property bool left_side
    property bool right_side
    property bool bottom_side
    property bool top_side

    hoverEnabled: true
    propagateComposedEvents: true
    z: 100

    acceptedButtons: Qt.LeftButton
    pressAndHoldInterval: 20

    Component.onCompleted: function() {
        if (!resizable) {
            resizable = parent
        }

        if (top_side) {
            width = parent.width
            height = 5
            anchors.top = parent.top
            cursorShape = Qt.SizeVerCursor
        } else if (bottom_side) {
            width = parent.width
            height = 5
            anchors.bottom = parent.bottom 
            cursorShape = Qt.SizeVerCursor
        } else if (right_side) {
            width = 5
            height = parent.height
            anchors.right = parent.right
            cursorShape = Qt.SizeHorCursor
        } else if (left_side) {
            width = 5
            height = parent.height
            anchors.left = parent.left
            cursorShape = Qt.SizeHorCursor
        }
    }

    onPressAndHold: function() {
        let edge = Qt.LeftEdge
        if (top_side) {
            edge = Qt.TopEdge
        } else if (bottom_side) {
            edge = Qt.BottomEdge
        } else if (right_side) {
            edge = Qt.RightEdge
        }
        resizable.startSystemResize(edge)
    }
}

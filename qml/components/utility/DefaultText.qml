import QtQuick

import ConstantsPackage

Text {
    color: Constants.fontColor
    font.pixelSize: Constants.fontSize
}
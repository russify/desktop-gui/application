import QtQuick

Canvas {
    id: canvas
    antialiasing: true

    property color color: "white"
    property bool fill: true

    property color stroke_color:  Qt.darker(color, 1.4)
    property bool stroke: false

    property real alpha: 1.0

    property int radius: 0
    property int top_right_radius: radius
    property int top_left_radius: radius
    property int bottom_right_radius: radius
    property int bottom_left_radius: radius

    property bool use_gradient: false
    // array of arrays: [[point, color], [point1, color1], ...]
    property variant gradient_stops
    property point gradient_begin: Qt.point(0, width)
    property point gradient_end: Qt.point(width, height)

    onPaint: {
        var ctx = getContext("2d");
        ctx.save();

        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.strokeStyle = canvas.stroke_color;
        ctx.lineWidth = canvas.lineWidth;
        ctx.globalAlpha = canvas.alpha;

        if (use_gradient) {
            let gradient = ctx.createLinearGradient(gradient_begin.x, gradient_begin.y, gradient_end.x, gradient_end.y);
            for (let index = 0; index < gradient_stops.length; ++index) {
                gradient.addColorStop(gradient_stops[index][0], gradient_stops[index][1]);
            }

            ctx.fillStyle = gradient;
        } else {
            ctx.fillStyle = canvas.color;
        }

        ctx.beginPath();
        ctx.moveTo(top_left_radius, 0);

        // draw top right corner
        ctx.lineTo(width-top_right_radius, 0);
        if (top_right_radius > 0) {
            ctx.arcTo(width, 0, width, top_right_radius, top_right_radius);
        }

        // draw bottom right corner
        ctx.lineTo(width, height-bottom_right_radius); // right side
        if (bottom_right_radius > 0) {
            ctx.arcTo(width, height, width-bottom_right_radius, height, bottom_right_radius);
        }

        // draw bottom left corner
        ctx.lineTo(bottom_left_radius, height); // bottom side
        if (bottom_left_radius > 0) {
            ctx.arcTo(0, height, 0, height-bottom_left_radius, bottom_left_radius);
        }
                
        // draw top left corner
        ctx.lineTo(0, top_left_radius); // left side
        if (top_left_radius > 0) {
            ctx.arcTo(0, 0, top_left_radius, 0, top_left_radius);
        }

        ctx.closePath();
                
        if (canvas.fill) {
            ctx.fill();
        }


        if (canvas.stroke) {
            ctx.stroke();
        }

        ctx.restore();
    }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        propagateComposedEvents: true
        z: -100

        onClicked: () => {
            canvas.focus = true;
        }
    }
}
import QtQuick
import QtQuick.Controls

Button {
    id: root

    contentItem: DefaultText {
        text: root.text

        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

        background: RoundShape {
        gradient_stops: [[0.0, "#63D5E3"], [1.0, "#E93AF5"]]
        gradient_begin: Qt.point(0, 0)
        gradient_end: Qt.point(width, 0)
        use_gradient: true
    } // background

    
} // button
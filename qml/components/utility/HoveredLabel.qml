import QtQuick

import ConstantsPackage

DefaultText {
    id: root
    required property color mainColor
    property alias mouseArea: area
    color: mainColor

    MouseArea {
        id: area
        anchors.fill: parent
        hoverEnabled: true

        onEntered: function() {
            color = Constants.hoveredTextColor;
        }

        onExited: function() {
            color = mainColor;
        }
    }
}
import QtQuick
import QtQuick.Controls
import ConstantsPackage

ComboBox {
    id: root
    property variant itemTokenizer: (item) => item
    property int popupHeight

    QtObject {
        id: data
        property string search: ""
    }

    delegate: ItemDelegate {
        width: root.width
        contentItem: Label {
            text: itemTokenizer(root.model[index])
            background: Rectangle {
                color: "#EFB3DF"
            }
                            
            color: Constants.fontColor
            font.pixelSize: Constants.fontSize                            
        } // contentItem

        background: Rectangle {
            color: root.highlightedIndex === index ? Qt.rgba(0.933, 0.699, 0.871, 0.35) : "#EFB3DF"
        } // background

        highlighted: root.highlightedIndex === index
    } // deletable

    contentItem: DefaultText {
        text: root.displayText
    } // contentItem

    background: Rectangle {
        color: Constants.white
    } // background

    popup: Popup {
        y: root.height - 1
        width: root.width
        height: root.popupHeight ? Math.min(root.popupHeight, 150) : Math.min(implicitHeight, 150)
        implicitHeight: contentItem.implicitHeight
        padding: 1

        contentItem: ListView {
            clip: true
            implicitHeight: contentHeight
            model: root.popup.visible ? root.delegateModel : null
            currentIndex: root.highlightedIndex

            ScrollIndicator.vertical: ScrollIndicator { }
        } // contentItem

        background: Rectangle {
            color: "transparent"
            border {
                color: "gray"
                width: 2
            }
        } // background
    } // Popup
} // ComboBox
import QtQuick
import ConstantsPackage

import "../utility"

Rectangle {
    Rectangle {
        id: search

        width: parent.width * 0.3
        color: "transparent"

        Image {
            id: image
            anchors {
                left: parent.left

                verticalCenter: parent.verticalCenter
            }
            width: 20
            height: 20
            mipmap: true

            source: "qrc:///images/images/nav-bar/search.svg"
        }

        TextFieldWithBottomLine {
            anchors {
                left: image.right
                right: parent.right

                verticalCenter: parent.verticalCenter

                leftMargin: parent.width * 0.05
            }

            placeholderText: "search..."
            height: 20
        }
    }

    Image {
        id: label
        anchors {
            verticalCenter: parent.verticalCenter
            left: search.left

            leftMargin: parent.width * 0.5 - width / 2
        }

        mipmap: true
        width: 50
        height: parent.height < 60 ? parent.height : 60
        source: "qrc:///images/images/nav-bar/headphones.svg"
    }

    HoveredLabel {
        id: mymusic

        text: "my music"
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 30
        mainColor: Constants.white

        anchors {
            left: label.right

            leftMargin: parent.width * 0.1
        }

        Rectangle {
            anchors {
                top: parent.bottom
                left: parent.left
                right: parent.right
            }

            color: Constants.white
            height: 1
        }
    }

    HoveredLabel {
        id: profile

        text: "profile"
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 30
        mainColor: Constants.white

        anchors {
            left: mymusic.right

            leftMargin: parent.width * 0.1
        }

        width: parent.width * 0.1

        Rectangle {
            anchors {
                top: parent.bottom
                left: parent.left
                right: parent.right
            }

            color: Constants.white
            height: 1
        }
    }
}
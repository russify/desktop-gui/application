import QtQuick

 Rectangle {
    required property string image_source
    required property variant action
    required property size image_size

    color: "transparent"
    
    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        propagateComposedEvents: true

        acceptedButtons: Qt.LeftButton

        onEntered: function() {
            color = "lightgray"
        }

        onExited: function() {
            color = "transparent"
        }

        onPressed: action()

        Image {
            mipmap: true
            source: image_source

            height: image_size.height
            width: image_size.width

            anchors.centerIn: parent
        }
    }
}
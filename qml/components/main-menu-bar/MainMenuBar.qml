import QtQuick
import "../utility"

RoundShape {
    id: menu_rect

    required property variant window
    required property variant tray

    property double buttons_end_pos
    property size button_size
    property alias icon_size : cancel_button.image_size
    property double buttons_y

    property double spacing

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        propagateComposedEvents: true

        property bool locked_
        property variant click_pos_
        property bool show_maximized_
        property bool restored_
        
        property size normal_mode_size_

        Component.onCompleted: function() {
            locked_ = false
            restored_ = true
        }

        onPositionChanged: function(event) {
            if (locked_) {
                let global = mapToGlobal(event.x, event.y)
                if (global.y <= parent.height / 2) {
                    show_maximized_ = true
                } else {
                    show_maximized_ = false
                    if (!restored_) {
                        window.showNormal()
                        
                        restored_ = true

                        window.width = normal_mode_size_.width
                        window.height = normal_mode_size_.height
                        return
                    }
                }

                let delta_x = event.x - click_pos_.x
                let delta_y = event.y - click_pos_.y
                window.x += delta_x
                window.y += delta_y
            }
        }

        onPressed: function(event) {
            locked_ = true
            click_pos_ = Qt.point(event.x, event.y)
        }

        onReleased: function(){
            locked_ = false 
            if (show_maximized_ && window.visibility !== Window.Maximized) {
                normal_mode_size_.width = window.width
                normal_mode_size_.height = window.height
                restored_ = false

                window.showMaximized()
            }
        }

        MainMenuBarItem {
            id: cancel_button

            height: button_size.height
            width: button_size.width

            x: menu_rect.buttons_end_pos ? menu_rect.buttons_end_pos : menu_rect.width - width - 10
            y: buttons_y ? buttons_y : 0

            image_source: "qrc:///images/images/main-menu-bar/cancel.svg"
            action: function() {
                Qt.quit()
            }
        }

        MainMenuBarItem {
            id: expand_button

            height: cancel_button.height
            width: cancel_button.width
            
            anchors.right: cancel_button.left
            anchors.rightMargin: spacing ? spacing : 0

            y: buttons_y ? buttons_y : 0

            image_size: cancel_button.image_size
            image_source: "qrc:///images/images/main-menu-bar/expand.svg"
            action: function() {
                if (window.visibility ===  Window.Windowed) {
                    window.showMaximized()
                } else if (window.visibility === Window.Maximized) {
                    window.showNormal()
                }
            }
        }

        MainMenuBarItem {
            id: minimize_button

            height: cancel_button.height
            width: cancel_button.width
            
            anchors.right: expand_button.left
            anchors.rightMargin: expand_button.rightMargin

            y: buttons_y ? buttons_y : 0
            
            image_size: cancel_button.image_size
            image_source: "qrc:///images/images/main-menu-bar/minimize.svg"
            action: function() {
                if (window.visibility !== Window.Minimized) {
                    window.showMinimized()
                    window.flags = Qt.SubWindow;
                    tray.show()
                }
            }
        }
    }
}

function formatTextWithMaxLength(value, max_length) {
    if (max_length) {
        if (value.length > max_length) {
            return value.substring(0, max_length) + "..."
        }
    }

    return value;
}

function formatDuration(value, leading_zeroes) {
    let result;

    let minutes = value / 60 >> 0;
    let seconds = value % 60;

    if (leading_zeroes) {
        if (minutes < 10) {
            result = "0" + minutes;
        } else {
            result = minutes;
        }

        result += ":"

        if (seconds < 10) {
            result += "0" + seconds;
        } else {
            result += seconds;
        }
    } else {
        result = minutes + ":" + seconds;
    }

    return result;
}

function rightShift(value, count) {
    count %= value.length;

    let symbols = "";
    for (let index = value.length - count; index < value.length; ++index) {
        symbols += value[index];
    }

    return symbols + value.substring(0, value.length - count)
}

function leftShift(value, count) {
    count %= value.length;

    let symbols = "";
    for (let index = 0; index < count; ++index) {
        symbols += value[index];
    }
    return value.substring(count) + symbols;
}